# TestingMod
### A Testing Mod for Minecraft 1.12.2 using the Forge API. 

This mod is a hodgepodge of different changes to Minecraft. It is simply a testing ground for me while I experiment with Minecraft modding.

Current modifications:
* Added mineable Ruby blocks naturally spawning Ore.
* Added craftable Ruby armor, tools, and weapons.
* Added stone trees that spawn in plains biomes.
* Added Magic Pears and Not Magic Apples.