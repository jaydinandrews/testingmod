package com.xmanlopez13.testingmod.plants;

import net.minecraft.block.BlockMushroom;
import net.minecraft.block.SoundType;
import net.minecraft.creativetab.CreativeTabs;

public class MagicMushroom extends BlockMushroom {

    public MagicMushroom() {
        super();
        setCreativeTab(CreativeTabs.FOOD);
        setSoundType(SoundType.PLANT);
        setLightLevel(0.2225F);
    }
}
