package com.xmanlopez13.testingmod.items.food;

import com.xmanlopez13.testingmod.util.IHasModel;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class FoodEffectBase extends FoodBase implements IHasModel {

    PotionEffect effect;
    boolean isShiny;

    public FoodEffectBase(String name, int amount, float saturation, boolean isAnimalFood, PotionEffect effect, boolean isShiny) {
        super(name, amount, saturation, isAnimalFood);
        setAlwaysEdible();

        this.effect = effect;
        this.isShiny = isShiny;
    }

    //Only applies effect if on local game, not remote due to lag issues
    @Override
    protected void onFoodEaten(ItemStack stack, World worldIn, EntityPlayer player) {
        if (!worldIn.isRemote) {
            player.addPotionEffect(new PotionEffect(effect.getPotion(), effect.getDuration(), effect.getAmplifier(), effect.getIsAmbient(), effect.doesShowParticles()));
        }
    }

    //Allows for custom food items to have potion effect properties without being shiny like enchanted items
    @SideOnly(Side.CLIENT)
    public boolean hasEffect(ItemStack stack) {
        boolean shine = true;
        if (!this.isShiny) {
            shine = false;
        }
        return shine;
    }
}
