package com.xmanlopez13.testingmod.util;

/* Reference class separate from the Main class for easy editing. Used to help FML and the user determine compatibility
 * and release versioning. */
public class Reference {

    public static final String MOD_ID = "tm";
    public static final String NAME = "Testing Mod";
    public static final String VERSION = "1.0.0";
    public static final String ACCEPTED_VERSIONS = "[1.12.2]";
    public static final String CLIENT_PROXY_CLASS = "com.xmanlopez13.testingmod.proxy.ClientProxy";
    public static final String COMMON_PROXY_CLASS = "com.xmanlopez13.testingmod.proxy.CommonProxy";

}
