package com.xmanlopez13.testingmod.world;

import com.xmanlopez13.testingmod.init.ModBlocks;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.biome.BiomePlains;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraft.world.gen.feature.WorldGenMinable;
import net.minecraft.world.gen.feature.WorldGenTrees;
import net.minecraft.world.gen.feature.WorldGenerator;
import net.minecraftforge.fml.common.IWorldGenerator;

import java.util.Random;

public class ModWorldGen implements IWorldGenerator {

    //TODO Remove this stone_tree WorldGenTrees Object, create in another package and then call the object in the
    // generateOverworld method.
    private WorldGenTrees stone_tree = new WorldGenTrees(false, 5, Blocks.COBBLESTONE.getDefaultState(), Blocks.STONE.getDefaultState(), false);

    /* Overrides IWorldGenerator Interface and calls methods for generating each dimension.*/
    @Override
    public void generate(Random random, int chunkX, int chunkZ, World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider) {
        if (world.provider.getDimension() == 0) {
            generateOverworld(random, chunkX, chunkZ, world, chunkGenerator, chunkProvider);
        }
    }

    /* Calls all Overworld generator methods with specific blocks, ores, plants, etc. */
    private void generateOverworld(Random random, int chunkX, int chunkZ, World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider) {
        //Ores
        generateOre(ModBlocks.RUBY_ORE.getDefaultState(), world, random, chunkX * 16, chunkZ * 16, 5, 35, random.nextInt(3) + 4, 8);
        //Trees
        if (world.getBiomeForCoordsBody(new BlockPos(chunkX * 16, 0, chunkZ * 16)) instanceof BiomePlains) {
            generateTree(stone_tree, world, random, chunkX, chunkZ, 8);
        }
    }

    /* Makes a new BlockPos randomized from an x and z parameter and a y coordinate from somewhere within the min and max
     * values for ore spawn. Makes a new generator with an ore block and the size range. Repeats a number times with in
     * a cubed chunk then moves on to new chunk. */
    private void generateOre(IBlockState ore, World world, Random random, int x, int z, int minY, int maxY, int size, int chances) {
        int deltaY = maxY - minY;
        for (int i = 0; i < chances; i++) {
            BlockPos pos = new BlockPos(x + random.nextInt(16), minY + random.nextInt(deltaY), z + random.nextInt(16));

            WorldGenMinable generator = new WorldGenMinable(ore, size);
            generator.generate(world, random, pos);
        }
    }

    /* Takes chunk coordinate, multiplies it by 16 the adds random number to get block pos x and z.
     * Then it will bitshift those coordinates to get the chunk, and then the height of the chunk as the y coordinate.
     * Subtracted by 1 because the WorldGenTrees generate() method adds one to ensure the tree is on top of the block.
     * Finally generate the trees at that position and restart the loop. */
    private void generateTree(WorldGenerator generator, World world, Random random, int chunkX, int chunkZ, int amountPerChunk) {
        for (int i = 0; i < amountPerChunk; i++) {
            int x = chunkX * 16 + random.nextInt(16);
            int z = chunkZ * 16 + random.nextInt(16);
            int y = world.getChunkFromChunkCoords(x >> 4, z >> 4).getHeight(new BlockPos(x & 15, 0, z & 15)) - 1;
            generator.generate(world, random, new BlockPos(x, y, z));
        }
    }
}
