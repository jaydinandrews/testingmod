package com.xmanlopez13.testingmod.init;

import com.xmanlopez13.testingmod.items.ItemBase;
import com.xmanlopez13.testingmod.items.armor.ArmorBase;
import com.xmanlopez13.testingmod.items.food.FoodEffectBase;
import com.xmanlopez13.testingmod.items.tools.*;
import com.xmanlopez13.testingmod.util.Reference;
import net.minecraft.init.MobEffects;
import net.minecraft.init.SoundEvents;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.*;
import net.minecraft.potion.PotionEffect;
import net.minecraftforge.common.util.EnumHelper;

import java.util.ArrayList;
import java.util.List;

public class ModItems {

    //Adds all of the custom items to an array for easy access and reference in other classes
    public static final List<Item> ITEMS = new ArrayList<Item>();

    //Materials for making custom tools, weapons, and armor
    public static final Item.ToolMaterial MATERIAL_RUBY = EnumHelper.addToolMaterial("material_ruby", 3, 500, 7.0F, 2.5F, 12);
    public static final ItemArmor.ArmorMaterial ARMOR_MATERIAL_RUBY = EnumHelper.addArmorMaterial("armor_material_ruby", Reference.MOD_ID + ":ruby", 14,
            new int[]{2, 5, 7, 3}, 12, SoundEvents.ITEM_ARMOR_EQUIP_DIAMOND, 0.0F);

    //Items
    public static final Item RUBY = new ItemBase("ruby");
    public static final Item SAPPHIRE = new ItemBase("sapphire");

    //Tools
    public static final ItemSword RUBY_SWORD = new ToolSword("ruby_sword", MATERIAL_RUBY);
    public static final ItemSpade RUBY_SHOVEL = new ToolSpade("ruby_shovel", MATERIAL_RUBY);
    public static final ItemPickaxe RUBY_PICKAXE = new ToolPickaxe("ruby_pickaxe", MATERIAL_RUBY);
    public static final ItemAxe RUBY_AXE = new ToolAxe("ruby_axe", MATERIAL_RUBY);
    public static final ItemHoe RUBY_HOE = new ToolHoe("ruby_hoe", MATERIAL_RUBY);

    //Armor
    public static final Item RUBY_HELMET = new ArmorBase("ruby_helmet", ARMOR_MATERIAL_RUBY, 1, EntityEquipmentSlot.HEAD);
    public static final Item RUBY_CHESTPLATE = new ArmorBase("ruby_chestplate", ARMOR_MATERIAL_RUBY, 1, EntityEquipmentSlot.CHEST);
    public static final Item RUBY_LEGGINGS = new ArmorBase("ruby_leggings", ARMOR_MATERIAL_RUBY, 2, EntityEquipmentSlot.LEGS);
    public static final Item RUBY_BOOTS = new ArmorBase("ruby_boots", ARMOR_MATERIAL_RUBY, 1, EntityEquipmentSlot.FEET);

    //Food
    //public static final Item MAGIC_PEAR = new FoodBase("magic_pear", 4, 2.4F, false);
    public static final Item MAGIC_PEAR = new FoodEffectBase("magic_pear", 4, 2.4F, false, new PotionEffect(MobEffects.NAUSEA, 6000, 0, true, false), false);
    public static final Item NOT_MAGIC_APPLE = new FoodEffectBase("not_magic_apple", 4, 2.4F, false, new PotionEffect(MobEffects.HASTE, 6000, 0, true, false), true);

}
