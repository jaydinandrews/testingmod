package com.xmanlopez13.testingmod.init;

import com.xmanlopez13.testingmod.blocks.RubyBlock;
import com.xmanlopez13.testingmod.blocks.RubyOre;
import com.xmanlopez13.testingmod.blocks.SapphireBlock;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

import java.util.ArrayList;
import java.util.List;

public class ModBlocks {

    //Adds all of the custom blocks to an array for easy access and reference in other classes
    public static final List<Block> BLOCKS = new ArrayList<Block>();

    //Building Blocks
    public static final Block RUBY_BLOCK = new RubyBlock("ruby_block", Material.IRON);
    public static final Block SAPPHIRE_BLOCK = new SapphireBlock("sapphire_block", Material.IRON);

    //Ores
    public static final Block RUBY_ORE = new RubyOre("ruby_ore", Material.ROCK);
}
