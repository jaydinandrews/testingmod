package com.xmanlopez13.testingmod.blocks;

import com.xmanlopez13.testingmod.init.ModItems;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.Item;

import java.util.Random;

public class RubyOre extends BlockBase {

    public RubyOre(String name, Material material) {
        super(name, material);
        setSoundType(SoundType.STONE);
        setHardness(3.0F);
        setResistance(15.0F);
        setHarvestLevel("pickaxe", 2);
    }

    //When mined, drops Ruby item instead of ore
    @Override
    public Item getItemDropped(IBlockState state, Random rand, int fortune) {
        return ModItems.RUBY;
    }

    //Mining Ruby Ore can drop 1-2 Rubies
    @Override
    public int quantityDropped(Random rand) {
        int max = 2;
        int min = 1;
        return rand.nextInt(max) + min;
    }
}
