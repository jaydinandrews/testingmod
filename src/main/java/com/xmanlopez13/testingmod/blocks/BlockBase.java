package com.xmanlopez13.testingmod.blocks;

import com.xmanlopez13.testingmod.Main;
import com.xmanlopez13.testingmod.init.ModBlocks;
import com.xmanlopez13.testingmod.init.ModItems;
import com.xmanlopez13.testingmod.util.IHasModel;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;

public class BlockBase extends Block implements IHasModel {

    public BlockBase(String name, Material material) {

        super(material);
        setUnlocalizedName(name);
        setRegistryName(name);
        setCreativeTab(CreativeTabs.BUILDING_BLOCKS);

        /* Adds the block to our array of blocks and items for the mod. */
        ModBlocks.BLOCKS.add(this);
        ModItems.ITEMS.add(new ItemBlock(this).setRegistryName(this.getRegistryName()));
    }

    /* Once a block is broken it is no longer a block its an item that can be used in inventory application. This method
     * registers those items as items in the game from the ItemID. */
    @Override
    public void registerModels() {
        Main.proxy.registerItemRenderer(Item.getItemFromBlock(this), 0, "inventory");
    }
}
