package com.xmanlopez13.testingmod;

import com.xmanlopez13.testingmod.init.ModRecipes;
import com.xmanlopez13.testingmod.proxy.CommonProxy;
import com.xmanlopez13.testingmod.util.Reference;
import com.xmanlopez13.testingmod.world.ModWorldGen;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;

@Mod(modid = Reference.MOD_ID, name = Reference.NAME, version = Reference.VERSION)
public class Main {

    @Instance
    public static Main instance;

    @SidedProxy(clientSide = Reference.CLIENT_PROXY_CLASS, serverSide = Reference.COMMON_PROXY_CLASS)
    public static CommonProxy proxy;

    /* This registers the modded World Generator as a preinitialization event to ensure the world is generated before
     * the rest of the game. */
    @EventHandler
    public static void PreInit(FMLPreInitializationEvent event) {
        GameRegistry.registerWorldGenerator(new ModWorldGen(), 3);
    }

    /* While the mod is being initialized, the ModRecipes init method is called to register and smelting recipes in game.*/
    @EventHandler
    public static void init(FMLInitializationEvent event) {
        ModRecipes.init();
    }

    @EventHandler
    public static void PostInit(FMLPostInitializationEvent event) {

    }
}
